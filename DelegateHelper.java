package co.bytemark.android.opentools.delegatehelper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

/**
 * <b>Location:</b>
 * <br>DelegateHelper.java must be located within co.bytemark.android.opentools.delegatehelper to be compatible with other Bytemark inc. opentools. 
 * <br>
 * <br><b>Description:</b>
 * <br>DelegateHelper adds functionality to the delegate design pattern by allowing java classes to implement optional delegate callback methods. 
 * <br>
 * <br>The DelegateHelper functions by performing a look-up for specified methods to determine if the delegate class conforms to the requested delegate method. If the requested method is found in the delegate class, it is stored in a HashMap for future access, and can then be invoked.   
 * <br> 
 * <br>Typical use of the DelegateHelper includes sub-classing the delegate helper to hold the requests that the delegate can make. The {@link respondsToMethod} function will determine if the delegate class conforms to the desired request. This function allows a default return value to be specified in the case that the delegate class does not provide support for the requested method.
 * 
 *@author kevin_rejko
 */

public class DelegateHelper {
	
	private Object m_oDelegate;
	private HashMap <String, Method> m_hmMethodMap;
	private Method[] _arrSortedMethods;
	
	public final Method NOT_FOUND = null;
	
	public DelegateHelper(Object _oDelegateInstance)
	{
		this.m_oDelegate = _oDelegateInstance;
		this.m_hmMethodMap = new HashMap <String, Method> ();
	}
	
	/**
	 * Will determine if the delegate class responds to a method with a given name. 
	 * 
	 * @param _szMethodName
	 * @return <code>true</code> if method is found<br><code>false</code> if delegate does not respond
	 */

	public boolean respondsToMethodWithName (String _szMethodName)
	{
		return ((getMethodWithName(_szMethodName)==NOT_FOUND) ? false :true);
	}
	
	
	/**
	 * Will attempt to retrieve the requested method from the HashMap of previously searched methods. If not found, a search for the method against the delegate class will be performed. 
	 * @param _szMethodName
	 * @return requested Method
	 */
	
	private Method getMethodWithName (String _szMethodName)
	{	
		if (m_hmMethodMap.containsKey(_szMethodName)){//Tries to pull method from the map
			return m_hmMethodMap.get(_szMethodName);
		}else{// If no method can be found in the map, it will search the base class to see if it exists. Saves method into map if found in base class
			Method _mFoundMethod = findMethodInDelegateClassWithParameters(_szMethodName);
			m_hmMethodMap.put(_szMethodName, _mFoundMethod);
			return _mFoundMethod;
		}
	}
	
	
	/**
	 * Will ensure the list of methods from the delegate object are in sorted order before performing a binary search for the requested method.
	 * @param _szMethodName
	 * @return requested Method
	 */
	
	private Method findMethodInDelegateClassWithParameters (String _szMethodName)
	{
		@SuppressWarnings("rawtypes")
		Class _cDelegateClass = m_oDelegate.getClass();
		
		//Get and sort array for binary search if not done, ensure methods are alphabetical before Java 7 
		if (_arrSortedMethods==null){
			_arrSortedMethods = _cDelegateClass.getMethods();	
			Arrays.sort(_arrSortedMethods, new Comparator<Method>() {
				   public int compare(Method m1, Method m2) {
					   return m1.getName().compareTo(m2.getName());
				   }
			});
		}
		
		return binarySearchForMethodNamed(_arrSortedMethods, _szMethodName);
	}
	
	
	/**
	 * Will begin a binary search on the passed method array for the requested method.
	 * @param _arrMethods
	 * @param _szMethodName
	 * @return requested Method
	 */
	
	public Method binarySearchForMethodNamed(Method[] _arrMethods, String _szMethodName) {
        int left = 0;
        int right = _arrMethods.length - 1;
        return binarySearchMethods(_arrMethods, _szMethodName, left, right);
        }
	
	/**
	 * Recursive binary search that locates requested method from the sorted array.
	 * @param _arrMethods
	 * @param _szMethodName
	 * @param left
	 * @param right
	 * @return requested Method
	 */

	private Method binarySearchMethods(Method[] _arrMethods, String _szMethodName, int left, int right) {
        if (right < left) {
                return NOT_FOUND;
        }

        int mid = (left + right) >>> 1;
        String _szArrayMethodName = _arrMethods[mid].getName();
        if (_szMethodName.compareTo(_szArrayMethodName)>0) {
                return binarySearchMethods(_arrMethods, _szMethodName, mid + 1, right);
        } else if (_szMethodName.compareTo(_szArrayMethodName)<0) {
                return binarySearchMethods(_arrMethods, _szMethodName, left, mid - 1);
        } else {
                return _arrMethods[mid];
        }               
}
	
	
	/**
	 * Will perform the requested delegate method and return the response to the requestor. 
	 * @param _szMethodName
	 * @param _arrParameters
	 * @return Object returned from requested delegate method 
	 */
	
	public Object performMethodNamedWithParameters (String _szMethodName, Object _arrParameters[]){
		Method method = getMethodWithName (_szMethodName);
		if (method != NOT_FOUND){
			try {
           	 return method.invoke(m_oDelegate, _arrParameters);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	
}


